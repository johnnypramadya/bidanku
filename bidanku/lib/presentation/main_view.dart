import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:bidanku/presentation/routes/router.gr.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =  FlutterLocalNotificationsPlugin();


  final firebaseMessaging = FirebaseMessaging();
  String token = '';

  static Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) {
    debugPrint('onBackgroundMessage: $message');
    if (message.containsKey('data')) {
      String name = '';
      String age = '';
      if (Platform.isIOS) {
        name = message['name'];
        age = message['age'];
      } else if (Platform.isAndroid) {
        var data = message['data'];
        name = data['name'];
        age = data['age'];
      }

      debugPrint('onBackgroundMessage: name: $name & age: $age');
    }
    return null;
  }


  @override
  void initState() {
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: onSelectNotification);


    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        debugPrint('onMessage: $message');
        getDataFcm(message);
        _showNotificationWithDefaultSound(message);

      },
      onBackgroundMessage: onBackgroundMessage,
      onResume: (Map<String, dynamic> message) async {
        debugPrint('onResume: $message');
        getDataFcm(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        debugPrint('onLaunch: $message');
        getDataFcm(message);
      },
    );
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: true),
    );
    firebaseMessaging.onIosSettingsRegistered.listen((settings) {
      debugPrint('Settings registered: $settings');
    });
    firebaseMessaging.getToken().then((token) => setState(() {
          this.token = token;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bidanku'),
        actions: [
          buildIconButton(context),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView.count(
            crossAxisCount: 3,
            children: <Widget>[
              Card(
                  child: InkWell(
                splashColor: Colors.green.withAlpha(30),
                onTap: () {
                  ExtendedNavigator.of(context).push(Routes.bidanView);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 100,
                    child: Column(children: [
                      Image.asset(
                        'assets/images/pregnant_woman.png',
                        height: 40,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Buat Janji Bidan',
                        textAlign: TextAlign.center,
                      ),
                    ]),
                  ),
                ),
              )),
              Card(
                  child: InkWell(
                splashColor: Colors.green.withAlpha(30),
                onTap: () {
                  ExtendedNavigator.of(context).push(Routes.chatView);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 100,
                    child: Column(children: [
                      Image.asset(
                        'assets/images/chat.png',
                        height: 40,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Chat dengan Bidan',
                        textAlign: TextAlign.center,
                      ),
                    ]),
                  ),
                ),
              )),
              Card(
                  child: InkWell(
                splashColor: Colors.green.withAlpha(30),
                onTap: () {
                  ExtendedNavigator.of(context).push(Routes.artikelView);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 100,
                    child: Column(children: [
                      Image.asset(
                        'assets/images/medical_news.png',
                        height: 40,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'Artikel',
                        textAlign: TextAlign.center,
                      )
                    ]),
                  ),
                ),
              )),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildIconButton(BuildContext context) {
    return Stack(
      children: <Widget>[
        CupertinoButton(
            padding: EdgeInsets.only(right: 8),
            child: Icon(
              Icons.notifications,
              size: 28,
              color: Colors.white,
            ),
            onPressed: () {}),
      ],
    );
  }

  void getDataFcm(Map<String, dynamic> message) {
    String name = '';
    String age = '';
    if (Platform.isIOS) {
      name = message['name'];
      age = message['age'];
    } else if (Platform.isAndroid) {
      var data = message['data'];
      name = data['name'];
      age = data['age'];
    }
    debugPrint('getDataFcm: name: $name & age: $age');
  }

  Future onSelectNotification(String payload) async {
    ExtendedNavigator.of(context).push(Routes.mainView);
  }


  Future _showNotificationWithDefaultSound(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'Bidanku',
      'Bidanku',
      'Bidanku app',
      importance: Importance.high,
      priority: Priority.high,
      ticker: 'ticker',
      styleInformation: BigTextStyleInformation(''),
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        platformChannelSpecifics);
  }
}
