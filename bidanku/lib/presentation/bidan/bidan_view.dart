import 'package:bidanku/presentation/bidan/widget/bidan_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BidanView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(''),
      ),
      body: SafeArea(
        child: Container(
          child: ListView.separated(
            padding: EdgeInsets.only(bottom: 100),
            shrinkWrap: true,
            itemCount: 10,
            itemBuilder: (context, index) => BidanItemWidget(),
              separatorBuilder: (context, index) {
                return SizedBox(height: 4,);
              }
          ),
        ),
      ),
    );
  }
}
