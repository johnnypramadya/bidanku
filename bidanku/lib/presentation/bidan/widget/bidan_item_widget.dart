import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BidanItemWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
      splashColor: Colors.green.withAlpha(30),
      onTap: () {},
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 100,
          child: Row(children: [
            Image.asset(
              'assets/images/doctor.png',
              height: 100,
            ),
            SizedBox(
              height: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Dr. Johnny Deep',
                  textAlign: TextAlign.center,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.star,
                      color: Colors.yellow[700],
                    ),
                    Text(
                      '4.7',
                    )
                  ],
                ),
              ],
            )
          ]),
        ),
      ),
    ));
  }
}
