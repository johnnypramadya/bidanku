import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ArtikelItemWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
      splashColor: Colors.green.withAlpha(30),
      onTap: () {},
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 180,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Center(
              child: Image.asset(
                'assets/images/beautiful_pregnant.png',
                height: 100,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '8 Asupan Janin',
                  textAlign: TextAlign.start,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  'Bidanku | Sabtu 6 Maret 2021',
                  textAlign: TextAlign.start,
                  style: TextStyle(fontWeight: FontWeight.normal),
                ),
              ],
            )
          ]),
        ),
      ),
    ));
  }
}
