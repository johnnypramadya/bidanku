import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ChatView extends StatelessWidget {
  final messageInputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Chat'),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: [
                    Bubble(
                      margin: BubbleEdges.fromLTRB(16, 4, 72, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topLeft,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.leftTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 237, 237, 249),
                      child: Text('Heloo'),
                    ),
                    Bubble(
                      margin: BubbleEdges.fromLTRB(16, 4, 72, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topLeft,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.leftTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 237, 237, 249),
                      child: Text('Can i help you ?'),
                    ),
                    Bubble(
                      margin: BubbleEdges.fromLTRB(72, 4, 16, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topRight,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.rightTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 40, 140, 213),
                      child: Text('Can i have some',style:TextStyle(color: Colors.white),),
                    ),
                    Bubble(
                      margin: BubbleEdges.fromLTRB(72, 4, 16, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topRight,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.rightTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 40, 140, 213),
                      child: Text('Can i have some',style:TextStyle(color: Colors.white),),
                    ),
                    Bubble(
                      margin: BubbleEdges.fromLTRB(72, 4, 16, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topRight,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.rightTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 40, 140, 213),
                      child: Text('Can i have some',style:TextStyle(color: Colors.white),),
                    ),
                    Bubble(
                      margin: BubbleEdges.fromLTRB(72, 4, 16, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topRight,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.rightTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 40, 140, 213),
                      child: Text('Can i have some',style:TextStyle(color: Colors.white),),
                    ),
                    Bubble(
                      margin: BubbleEdges.fromLTRB(72, 4, 16, 4),
                      padding: BubbleEdges.symmetric(vertical: 8, horizontal: 8),
                      alignment: Alignment.topRight,
                      stick: true,
                      elevation: 0,
                      nip: BubbleNip.rightTop,
                      nipOffset: 8,
                      nipWidth: 8,
                      nipHeight: 12,
                      color: Color.fromARGB(255, 40, 140, 213),
                      child: Text('Can i have some',style:TextStyle(color: Colors.white),),
                    ),
                  ],
                )
              ),
              _buildChatTextField(context),
            ],
          ),
        ),
      ),
    );
  }

  _buildChatTextField(BuildContext context) {
    return Container(
      height: 56,
      width: double.infinity,
      padding: EdgeInsets.all(8),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Flexible(
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width,
                maxWidth: MediaQuery.of(context).size.width,
                minHeight: 28.0,
                maxHeight: 136.0,
              ),
              child: Scrollbar(
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color:  Color.fromARGB(255, 237, 237, 249),
                      width: 1,
                    ),
                    color:  Color.fromARGB(255, 237, 237, 249),
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: TextFormField(
                    cursorColor: Color(0xFF18A4E0),
                    keyboardType: TextInputType.multiline,
                    maxLines: 5,
                    onChanged: (_) {},
                    controller: messageInputController,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(
                          top: 2.0, left: 13.0, right: 13.0, bottom: 2.0),
                      hintText: "Write a message...",
                      hintStyle: TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          IconButton(
            icon: Image.asset(
              'assets/images/icon_attachment.png',
              fit: BoxFit.fill,
              width: 22,
              height: 22,
            ),
            onPressed: () {},
          ),
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.only(right: 12),
              decoration: new BoxDecoration(
                  color: Color(0xFF18A4E0),
                  borderRadius: BorderRadius.all(Radius.circular(32))),
              child: Image.asset(
                'assets/images/icon_send.png',
                fit: BoxFit.fill,
                width: 22,
                height: 22,
              ),
            ),
          )
        ],
      ),
    );
  }

}





