// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../artikel/artikel_view.dart';
import '../auth/sign_in_view.dart';
import '../bidan/bidan_view.dart';
import '../chat/chat_view.dart';
import '../main_view.dart';

class Routes {
  static const String signInView = '/';
  static const String mainView = '/main-view';
  static const String artikelView = '/artikel-view';
  static const String bidanView = '/bidan-view';
  static const String chatView = '/chat-view';
  static const all = <String>{
    signInView,
    mainView,
    artikelView,
    bidanView,
    chatView,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.signInView, page: SignInView),
    RouteDef(Routes.mainView, page: MainView),
    RouteDef(Routes.artikelView, page: ArtikelView),
    RouteDef(Routes.bidanView, page: BidanView),
    RouteDef(Routes.chatView, page: ChatView),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SignInView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SignInView(),
        settings: data,
      );
    },
    MainView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => MainView(),
        settings: data,
      );
    },
    ArtikelView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => ArtikelView(),
        settings: data,
      );
    },
    BidanView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => BidanView(),
        settings: data,
      );
    },
    ChatView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => ChatView(),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension RouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushSignInView() => push<dynamic>(Routes.signInView);

  Future<dynamic> pushMainView() => push<dynamic>(Routes.mainView);

  Future<dynamic> pushArtikelView() => push<dynamic>(Routes.artikelView);

  Future<dynamic> pushBidanView() => push<dynamic>(Routes.bidanView);

  Future<dynamic> pushChatView() => push<dynamic>(Routes.chatView);
}
