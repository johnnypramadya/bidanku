import 'package:auto_route/auto_route_annotations.dart';
import 'package:bidanku/presentation/artikel/artikel_view.dart';
import 'package:bidanku/presentation/auth/sign_in_view.dart';
import 'package:bidanku/presentation/bidan/bidan_view.dart';
import 'package:bidanku/presentation/chat/chat_view.dart';
import 'package:bidanku/presentation/main_view.dart';

@MaterialAutoRouter(
  generateNavigationHelperExtension: true,
  routes: <AutoRoute>[
    MaterialRoute(page: SignInView, initial: true),
    MaterialRoute(page: MainView),
    MaterialRoute(page: ArtikelView),
    MaterialRoute(page: BidanView),
    MaterialRoute(page: ChatView),
  ]
)
class $Router {}
